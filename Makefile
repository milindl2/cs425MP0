server: 
	g++ server.cpp -O3 -o server -lpthread

client: 
	g++ client.cpp -O3 -o client

clean:
	rm server
	rm client
