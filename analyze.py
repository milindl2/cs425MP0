import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

# Take file_name as input from command line
file_name = input("Enter file name: ")
data = pd.read_csv(file_name)


def get_period(df, start, end):
    selection = []
    start_bandwidth = end_bandwidth = 0
    for index, row in df.iterrows():
        if row.send_time > start and row.send_time <= end:
            selection.append(row)
            if len(selection) == 1:
                start_bandwidth = row.bandwidth
        if row.send_time > end:
            end_bandwidth = row.bandwidth
            break

    return (pd.DataFrame(selection), end_bandwidth - start_bandwidth)

def get_stats(df, start, end):

    (period, bandwidth) = get_period(data, start, end)
    diffs = period.send_time - period.recv_time

    th90 = np.percentile(diffs, 90)
    med = np.median(diffs)
    data_min = np.min(diffs)
    data_max = np.max(diffs)
    return [th90, med, data_min, data_max, bandwidth]

def get_graphs(): 
    bandwidths = []
    min_delay = []
    max_delay = []
    med_delay = []
    th90_delay = [] 
    min_time = int(data["recv_time"].min())
    max_time = int(data["send_time"].max())
    times = []
    for i in range(min_time, max_time): 
        (th90, med, data_min, data_max, bandwidth) = get_stats(data, i, i + 1)
        bandwidths.append(bandwidth / 1000)
        min_delay.append(data_min * 1000)
        max_delay.append(data_max * 1000)
        med_delay.append(med * 1000)
        th90_delay.append(th90 * 1000)
        times.append(i)

    fig, (ax, network_delay_ax) = plt.subplots(2)
    ax.plot(times, bandwidths)  
    ax.set(xlabel='time (s)', ylabel='Bandwidth (kb)',
       title='Average Bandwidth Over Time')
    ax.grid()

    network_delay_ax.set(xlabel='time (s)', ylabel='Network Delay(milliseconds)',
        title='Network Delay Over Time')

    network_delay_ax.grid()
    network_delay_ax.plot(times, min_delay, 'r', label = 'Minimum')
    network_delay_ax.plot(times, max_delay, 'b', label = 'Maximum') 
    network_delay_ax.plot(times, med_delay, 'g', label = 'Median')
    network_delay_ax.plot(times, th90_delay, 'k', label = '90th Percentile')

    network_delay_ax.legend(['Minimum', 'Maximum', 'Median', '90th Percentile']) 
    plt.show()

get_graphs()