// Client side C/C++ program to demonstrate Socket
// programming
#include <arpa/inet.h>
#include <stdio.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>
#include <string>
#include <iostream>
 
std::string get_msg(){
    std::string line;
    std::getline(std::cin, line);
    return line;
}

int main(int argc, char const* argv[])
{
    //argv[1] = name
    //argv[2] = addr
    //argv[3] = port
    if(argc != 4){
        std::cerr << "bad usage\n";
        return 1;
    }

    std::string name = argv[1];
    const char * addr = argv[2];
    int port = atoi(argv[3]);

    std::cerr << "name:" << name << " addr: " << addr << " port: " << port << '\n';

    int sock = 0, valread, client_fd;
    struct sockaddr_in serv_addr;
    const char* hello = "Hello from client";
    char buffer[1024] = { 0 };
    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        printf("\n Socket creation error \n");
        return -1;
    }
 
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(port);
 
    // Convert IPv4 and IPv6 addresses from text to binary
    // form
    if (inet_pton(AF_INET, addr, &serv_addr.sin_addr)
        <= 0) {
        printf(
            "\nInvalid address/ Address not supported \n");
        return -1;
    }
 
    if ((client_fd
         = connect(sock, (struct sockaddr*)&serv_addr,
                   sizeof(serv_addr)))
        < 0) {
        printf("\nConnection Failed \n");
        return -1;
    }
    send(sock, name.data(), name.size(), 0);
    while(true){
        std::string msg = get_msg();
        send(sock, msg.data(), msg.size(), 0);
        // valread = read(sock, buffer, 1024);
        // printf("%s\n", buffer);
    }
 
    // closing the connected socket
    close(client_fd);
    return 0;
}